package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Project;
import com.example.demo.repository.ProjectInterface;

@RestController
@RequestMapping("/projects")
public class ProjectController {
	@Autowired
	private ProjectInterface projectInterface;
	
	@PostMapping("/")
	public Project createProject(@RequestBody Project project) {
		Project project2 = projectInterface.save(project);
		return project2;
	}
	
	@GetMapping("/")
	public List<Project> getProjects() {
		return projectInterface.findAll();
	}
	
	@GetMapping("/{id}")
	public Project getProject(@PathVariable Integer id) {
		return projectInterface.findById(id).get();
	}
	
	@PutMapping("/{id}")
	public Project updateProject(@PathVariable Integer id,@RequestBody Project project ) {
		Project project2 = projectInterface.findById(id).get();
		project2.updateProject(project);
		return projectInterface.save(project2);
	}
	
	@DeleteMapping("/")
	public String deleteProject(@RequestBody Project project) {
		projectInterface.delete(project);
		return "deleted!!";
	}
}
