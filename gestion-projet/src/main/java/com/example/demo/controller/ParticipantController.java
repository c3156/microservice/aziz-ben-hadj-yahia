package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.feign.NotificationInterface;
import com.example.demo.model.Mail;
import com.example.demo.model.Participant;
import com.example.demo.model.Project;
import com.example.demo.model.User;
import com.example.demo.repository.ParticipantInterface;
import com.example.demo.repository.ProjectInterface;
import com.example.demo.repository.UserInterface;

@RestController
@RequestMapping("/participants")
public class ParticipantController {
	
	@Autowired
	private ParticipantInterface participantInterface;
	@Autowired
	private ProjectInterface projectInterface;
	@Autowired
	private UserInterface userInterface;
	@Autowired
    private CircuitBreakerFactory<?, ?> circuitBreakerFactory;
	@Autowired
	private NotificationInterface notificationInterface;
	
	@PostMapping("/")
	public Participant createParticipant(@RequestBody Participant participant) {
		CircuitBreaker circuitBreaker = circuitBreakerFactory.create("circuitbreaker");
		Project project = projectInterface.findById(participant.getProject().getId()).get();
		User user = userInterface.findById(participant.getUser().getId()).get();
		participant.setUser(user);
		participant.setProject(project);
		circuitBreaker.run(()->notificationInterface.sendSimpleEmail(new Mail(user.getEmail(), "vous êtes désormais participant au projet "+project.getName()+" en tant que "+participant.getRole(), "Participation à un nouveau projet")), throwable -> null);
		return participantInterface.save(participant);
	}
	
	@GetMapping("/")
	public List<Participant> getParticipants() {
		return participantInterface.findAll();
	}
	
	@GetMapping("/{id}")
	public Participant getParticipant(@PathVariable Integer id) {
		return participantInterface.findById(id).get();
	}
	
	@PutMapping("/{id}")
	public Participant updateParticipant(@PathVariable Integer id,@RequestBody String role) {
		Participant participant = participantInterface.findById(id).get();
		participant.setRole(role);
		return participantInterface.save(participant);
	}
	
	@DeleteMapping("/")
	public String deleteParticipant(@RequestBody Participant participant) {
		CircuitBreaker circuitBreaker = circuitBreakerFactory.create("circuitbreaker");
		participantInterface.delete(participant);
		circuitBreaker.run(()->notificationInterface.sendSimpleEmail(new Mail(participant.getUser().getEmail(), "vous n'êtes plus participant au projet "+participant.getProject().getName(), "Participant annulée")), throwable -> null);
		return "deleted!!";
	}
}
