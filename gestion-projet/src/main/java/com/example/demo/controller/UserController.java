package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.User;
import com.example.demo.repository.UserInterface;

@RestController
@RequestMapping("/users")
public class UserController {
	@Autowired
	private UserInterface userInterface ;
	
	@PostMapping("/")
	public User createUser(@RequestBody User user) {
		User user2 = userInterface.save(user);
		return user2;
	}
	
	@GetMapping("/")
	public List<User> getUsers() {
		return userInterface.findAll();
	}
	
	@GetMapping("/{id}")
	public User getUser(@PathVariable Integer id) {
		return userInterface.findById(id).get();
	}
	
	@PutMapping("/{id}")
	public User updateUser(@PathVariable Integer id,@RequestBody User user ) {
		User user2 = userInterface.findById(id).get();
		user2.updateUser(user);
		return userInterface.save(user2);
	}
	
	@DeleteMapping("/")
	public String deleteUser(@RequestBody User user) {
		userInterface.delete(user);
		return "deleted!!";
	}
}
