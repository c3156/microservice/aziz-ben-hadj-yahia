package com.example.demo.model;

public class Mail {
	private String eMail;
	private String message;
	private String sujet;
	
	
	public Mail(String eMail, String message, String sujet) {
		super();
		this.eMail = eMail;
		this.message = message;
		this.sujet = sujet;
	}
	public String getEMail() {
		return eMail;
	}
	public void setEMail(String eMail) {
		this.eMail = eMail;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getSujet() {
		return sujet;
	}
	public void setSujet(String sujet) {
		this.sujet = sujet;
	}
}
