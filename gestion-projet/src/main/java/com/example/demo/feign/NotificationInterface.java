package com.example.demo.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.model.Mail;

@FeignClient("notification")
public interface NotificationInterface {
	
	@PostMapping("/sendSimpleEmail/")
	String sendSimpleEmail(Mail mail);
}
