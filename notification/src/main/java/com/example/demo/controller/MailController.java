package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.model.Mail;

@Controller
public class MailController {
	@Autowired
    public JavaMailSender emailSender;

    @ResponseBody
    @PostMapping("/sendSimpleEmail")
    public String sendSimpleEmail(@RequestBody Mail mail ) {

        // Create a Simple MailMessage.
        SimpleMailMessage message = new SimpleMailMessage();
        
        message.setTo(mail.getEMail());
        message.setSubject(mail.getSujet());
        message.setText(mail.getMessage());

        // Send Message!
        this.emailSender.send(message);

        return "Email Sent!";
    }
}
