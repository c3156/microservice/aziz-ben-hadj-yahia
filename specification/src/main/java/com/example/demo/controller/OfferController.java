package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.feign.NotificationInterface;
import com.example.demo.feign.ProjectInterface;
import com.example.demo.model.Mail;
import com.example.demo.model.Offer;
import com.example.demo.model.Participant;
import com.example.demo.model.Project;
import com.example.demo.model.UserDTO;
import com.example.demo.repository.OfferRepository;

@RestController
@RequestMapping("/offers")
public class OfferController {
	@Autowired
	private OfferRepository offerRepository;
	@Autowired
    private CircuitBreakerFactory<?, ?> circuitBreakerFactory;
	@Autowired
	private NotificationInterface notificationInterface;
	@Autowired
	private ProjectInterface projectInterface;
	
	@PostMapping("/")
	public Offer createOffer(@RequestBody Offer offer){
	    CircuitBreaker circuitBreaker = circuitBreakerFactory.create("circuitbreaker");
	    UserDTO user = circuitBreaker.run(()->projectInterface.getUser(offer.getUserID()), throwable -> null);
	    if(user == null ) return null;
		return offerRepository.save(offer);
	}
	
	@GetMapping("/")
	public List<Offer> getOffer(){
		return offerRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public Offer getOffer(@PathVariable Integer id){
		return offerRepository.findById(id).get();
	}
	
	@PutMapping("/accept/{id}")
	public String acceptOffer(@PathVariable Integer id){
	    CircuitBreaker circuitBreaker = circuitBreakerFactory.create("circuitbreaker");
	    Offer offer = offerRepository.findById(id).get();
	    UserDTO user = circuitBreaker.run(()->projectInterface.getUser(offer.getUserID()), throwable -> null);
	    if(user == null) return "user not found";
	    Project project = circuitBreaker.run(()->projectInterface.createProject(new Project(0, offer.getSpecs().getTitre(), "")), throwable -> null);
	    if(project == null) return "couldn't create project";
	    Participant participant = circuitBreaker.run(()->projectInterface.createParticipant(new Participant(0, "admin", user, project)), throwable -> null);
	    if(participant == null) return "couldn't add user to project";
	    String msg = circuitBreaker.run(()->notificationInterface.sendSimpleEmail(new Mail(user.getEmail(), "votre offre numero" + offer.getId()+" a été acceptée et un nouveau projet est créer automatiquement pour vous", "Offre acceptée")), throwable -> "mail not sent");
	    offerRepository.delete(offer);

		return msg;
	}
	
	@PutMapping("/refuse/{id}")
	public String declineOffer(@PathVariable Integer id){
		CircuitBreaker circuitBreaker = circuitBreakerFactory.create("circuitbreaker");
	    Offer offer = offerRepository.findById(id).get();
	    UserDTO user = circuitBreaker.run(()->projectInterface.getUser(offer.getUserID()), throwable -> null);
	    String msg = "";
	    if(user != null)
	    msg = circuitBreaker.run(()->notificationInterface.sendSimpleEmail(new Mail(user.getEmail(), "votre offre numero" + offer.getId()+" a été refusée", "Offre refusée")), throwable -> "mail not sent");
	    else return "user not found";
	    offerRepository.delete(offer);
		return msg;
	}
	
	@PutMapping("/{id}")
	public Offer updateOffer(@PathVariable Integer id, @RequestBody Offer offer){
		Offer offer2 = offerRepository.findById(id).get();
		offer2.setMontant(offer.getMontant());
		return offerRepository.save(offer2);
	}
	
	@DeleteMapping("/{id}")
	public String deleteOffer(@PathVariable Integer id){
		offerRepository.deleteById(id);
		return "deleted";
	}
}
