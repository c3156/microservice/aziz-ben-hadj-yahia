package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.feign.ProjectInterface;
import com.example.demo.model.Specification;
import com.example.demo.model.UserDTO;
import com.example.demo.repository.SpecsRepository;


@RestController
@RequestMapping("/specs")
public class SpecsController {
	
	@Autowired
	private SpecsRepository specsRepository;
	@Autowired
	private ProjectInterface projectInterface;
	@Autowired
    private CircuitBreakerFactory<?, ?> circuitBreakerFactory;
	
	
	@PostMapping("/")
	public Specification createSpecs(@RequestBody Specification specification){
	    CircuitBreaker circuitBreaker = circuitBreakerFactory.create("circuitbreaker");
		UserDTO userDTO = circuitBreaker.run(() -> projectInterface.getUser(specification.getUserID()), Throwable -> null );
		if(userDTO == null) return null;
		return specsRepository.save(specification);
	}
	
	@GetMapping("/")
	public List<Specification> getSpecs(){
		return specsRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public Specification getSpec(@PathVariable Integer id){
		return specsRepository.findById(id).get();
	}
	
	@PutMapping("/{id}")
	public Specification updateSpec(@PathVariable Integer id, @RequestBody Specification specs){
		Specification specification = specsRepository.findById(id).get();
		specification.setTitre(specs.getTitre());
		return specsRepository.save(specification);
	}
	
	@DeleteMapping("/{id}")
	public String deleteSpec(@PathVariable Integer id){
		specsRepository.deleteById(id);
		return "deleted";
	}

	
	
}
