package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Offer;

public interface OfferRepository extends JpaRepository<Offer, Integer>{

}
