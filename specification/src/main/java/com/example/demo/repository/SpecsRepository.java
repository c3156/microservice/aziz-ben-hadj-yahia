package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Specification;

public interface SpecsRepository extends JpaRepository<Specification, Integer> {

}
