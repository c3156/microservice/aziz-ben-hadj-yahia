package com.example.demo.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.demo.model.Participant;
import com.example.demo.model.Project;
import com.example.demo.model.UserDTO;


@FeignClient("project")
public interface ProjectInterface {
	@GetMapping("/users/")
	List<UserDTO> getUsers();
	
	@GetMapping("/users/{id}")
	UserDTO getUser(@PathVariable Integer id);
	
	@PostMapping("/projects/")
	Project createProject(@RequestBody Project project);
	
	@PostMapping("/participants/")
	Participant createParticipant(@RequestBody Participant participant);
}
