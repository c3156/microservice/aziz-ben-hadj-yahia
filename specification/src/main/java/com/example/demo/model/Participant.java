package com.example.demo.model;


public class Participant {
	private Integer id;
	private String role;
	private UserDTO user;
	private Project project;
	
	
	public Participant(Integer id, String role, UserDTO user, Project project) {
		super();
		this.id = id;
		this.role = role;
		this.user = user;
		this.project = project;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public UserDTO getUser() {
		return user;
	}
	public void setUser(UserDTO user) {
		this.user = user;
	}
	public Project getProject() {
		return project;
	}
	public void setProject(Project project) {
		this.project = project;
	}
	
	
}
