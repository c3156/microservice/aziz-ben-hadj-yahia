package com.example.demo.model;


public class UserDTO {
	private Integer id;
	private String nom;
	private String username;
	private String email;
	
	
	public UserDTO() {
		super();
	}
	public UserDTO(Integer id, String nom, String username, String email) {
		super();
		this.id = id;
		this.nom = nom;
		this.username = username;
		this.email = email;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", nom=" + nom + ", username=" + username + ", email=" + email + "]";
	}
}
